Javascript Bootloader
======================

Bootloader is a minimal Javascript code that let you lazy load additional javascript files and other resources that your website would need from external sources, after the page have been
loaded. This is more or less inspired from Facebook, and you can read more about it here: https://www.facebook.com/video/video.php?v=596368660334&ref=mf
<br/><br/>
When you include multiple Javascript files - they will be added to the document in async mode.
<br/><br/>
Everything in this demo will be outputed as a Javascript alert() msg box.
