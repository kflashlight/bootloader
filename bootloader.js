/* Bootloader - version 1.0
   Kenny F. 2013
*/
(function (_win) {

    _win.Bootloader = function (args) {

        if (args.length == 0) return;

        try {
            var Opera = /opera/i.test(navigator.userAgent),
                _doc = document,
                js_loaded = [], // Maintain a list of javascript files we have already loaded to prevent duplicates
                inArray = function (a, b) {
                    for (var d = a.length; d--;) {
                        if (a[d] === b) return !0;
                    }
                    return !1;
                },
                InjectJS = function (url, callback) {

                    var head = _doc.getElementsByTagName("head")[0] || _doc.getElementsByTagName('body')[0],
                        script = _doc.createElement("script"),
                        firstScript = _doc.getElementsByTagName('script')[0],
                        loaded = !1;
                    script.async = !0;
                    script.type = "text/javascript";
                    script.id = "script" + Math.floor(Math.random() * 911); // Unique ID for each javascript file on each pagelet

                    // Hack for older Opera browsers. Some of them fires load event multiple times, even when the DOM is not ready yet.
                    // This have no impact on the newest Opera browsers, because they share the same engine as Chrome.

                    Opera && this.readyState && "complete" != this.readyState || (script.onload = function () {
                            loaded || (console.log("loaded " + url + ' - id:' + script.id), loaded = !0, callback && callback())
                        },

                        // Fall-back for older IE versions ( IE 6 & 7), they do not support the onload event on the script tag  

                        script.onreadystatechange = function () {
                            loaded || this.readyState && "loaded" !== this.readyState && "complete" !== this.readyState || (script.onerror = script.onload = script.onreadystatechange =
                                null, console.log("loaded " + url + ' - id:' + script.id), loaded = !0, head && script.parentNode && head.removeChild(script))
                        }, // Because of a bug in IE8, the src needs to be set after the element has been added to the document.
                        firstScript.parentNode.insertBefore(script, firstScript), script.src = url)

                    // Cross-browser onload 

                }, Loader = function (func) {
                    _win.addEventListener ? _win.addEventListener("load", func, !1) : _win.attachEvent && _win.attachEvent("onload", func)
                };
            if (typeof args == 'object') {

                if (args.external !== 'undefined') {
                    try {

                        // Async loading...:
                        (function asyncLoop(i) {
                            setTimeout(function () {

                                // If js file not injected allready, we inject it

                                if (!inArray(js_loaded, args.external[i - 1])) {

                                    // Push to the list list of javascript files we already have loaded
                                    js_loaded.push(args.external[i - 1]);

                                    InjectJS(args.external[i - 1], function () {}); // Empty callback function
                                }
                                --i && asyncLoop(i);

                            }, 10);
                        })(args.external.length);
                        
                    } catch (h) {
                        alert("Something went wrong!");
                    }
                }
                if (args.success !== 'undefined') {

                    // Loads the final function
                    Loader(args.success);
                }
            } else {
                if (typeof args != 'function') {
                    
                  // If the js file is not loaded yet, we load it
				   if (!inArray(js_loaded, args)) {
					
					 // Push to the list list of javascript files we already have loaded
				       js_loaded.push(args); 
					 // Load a single Javascript file
					   InjectJS(args, function () {}); 					 
					 } 
                } else {
                    // Load the called function
                    Loader(args);
                }
            }
            return this;

        } catch (e) {}
    }
})(window);